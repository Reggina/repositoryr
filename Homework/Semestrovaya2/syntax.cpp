#include "syntax.h"
#include <string.h>
#include <iostream>
#include <stack>
using std::stack;

void clear(char a[])
{
	for (int p=0; p<6; p++)
		a[p] = 0;
}

void init_program(Program &p) {
  create(p.in);
  create(p.commands);
}

bool create_commands(List<Lexeme> lexemes, Program& program) 
{
	move_first(program.commands);
	move_first(lexemes);
	stack<Lexeme> st;
	while (lexemes.cur != NULL)
	{
		int s = lexemes.cur->elem.type;
		Command *c = new Command;
		create(c->rpn);
		move_first(c->rpn);
		strcpy_s(c->var_result, 5, lexemes.cur->elem.var_name);
		move_next(lexemes);
		while (s != 3 && s != 4 && s != 5)
		{
			move_next(lexemes);
			s = lexemes.cur->elem.type;
			switch(s)
			{
			case 0:
			case 1:
				add_end(c->rpn, lexemes.cur->elem);
				break;
			case 11: 
				st.push(lexemes.cur->elem);
				break;
			case 8:
			case 10:
				st.push(lexemes.cur->elem);
				break;
			case 7:
			case 9:
				while (!st.empty() && (st.top().type == 8 || st.top().type == 10))
				{
					add_end(c->rpn, st.top());
					st.pop();
				}
				st.push(lexemes.cur->elem);
				break;
			case 12:
				while (st.top().type != 11)
				{
					add_end(c->rpn, st.top());
					st.pop();
				}
				st.pop();
				if (st.empty()) s = 5;
				break;
			case 3:
			case 4:
				while (!st.empty())
				{
					if (st.top().type != 11)
					{
					add_end(c->rpn, st.top());
					st.pop();
					}
					else return 0;
				}
				break;
			}
		
		}	
		if (s == 5) return 0;
		add_first(program.commands, *c);
		clear(c->var_result);
		create(c->rpn);
		move_first(c->rpn);
		move_next(lexemes);
	}
 return 1;
}

char* search(List<char[100]> l, char t[])
{
	char* res = NULL;
	move_first(l);

	while(!is_last(l))
	{
		if (strcmp(get_cur(l), t) == 0)
			res = t;
		move_next(l);
	}
	return res;	
}

void find_initialized(Program& program)
{
	List<char[100]> assigned;
	create(assigned);
	move_first(program.commands);
	
	while(!is_last(program.commands))
	{
		move_first(get_cur(program.commands).rpn);
		while(!is_last(get_cur(program.commands).rpn))
		{
			Lexeme l = get_cur(get_cur(program.commands).rpn);
			if (l.type == 0)
			{
				char* a = search(assigned, l.var_name);
				char* b = search(program.in, l.var_name);
				if (a == NULL && b == NULL)
					add_first1(program.in, l.var_name);
			}
			move_next(get_cur(program.commands).rpn);
		}
		add_first1(assigned, get_cur(program.commands).var_result);
		move_next(program.commands);
	}

}

bool syntax_analyzer(List<Lexeme> lexemes, Program& program)
{
	if (!create_commands(lexemes, program)) 
		return 0;
	find_initialized(program);
	return 1;
}

