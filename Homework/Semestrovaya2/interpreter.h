#ifndef _INTERPRETER_H_
#define _INTERPRETER_H_

#include "types.h"
#include "List.h"
#include <iostream>
#include "Stack.h"

using namespace std;

void interpret(istream& in, Program program, List<Var>& vars);



#endif