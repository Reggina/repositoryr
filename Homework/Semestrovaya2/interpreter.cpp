
#include "interpreter.h"
#include <string.h>

void add_first1(List<char[100]>& l, char elem[]) 
{
	Node<char[100]>* p = new Node<char[100]>;
	// p->elem = elem;
	strcpy_s(p->elem, 5, elem);
	p->next = l.head;
	if (l.head == NULL)
		l.last = p;
	l.head = p;
}

void read_vars(istream& in, List<char[100]> names, List<Var>& vars)
{
	move_first(names);
	while(!is_last(names))
	{
		Var v;
		strcpy_s(v.name, 5,get_cur(names));
		cout << get_cur(names) << '=';
		in >> v.value;
		add_first(vars, v);
		move_next(names);
	}
}

Var* look_up(List<Var> vars, char name[])
{
		Var* res = NULL;
	move_first(vars);

	while(!is_last(vars))
	{
		if (strcmp(vars.cur->elem.name, name) == 0)
		{
			res = new Var;
			strcpy_s(res->name , 5, name);
			res->value = vars.cur->elem.value;
		}
		move_next(vars);
	}
	return res;	
}

void run_command(char res[], List<Lexeme> rpn, List<Var>& vars)
{
	Stack<int> s;
	move_first(rpn);
	
	while (!is_last(rpn))
	{
		switch(get_cur(rpn).type)
		{
		case 0: 
			{
			Var* v = look_up(vars, get_cur(rpn).var_name);
			push(s, v->value);
			}
			break;
		case 1:
			push(s,get_cur(rpn).int_value);
			break;
		default:
			int a = pop(s);
			int b = pop(s);
			int r;
			if (get_cur(rpn).type == 7)
				r = a+b;
			if (get_cur(rpn).type == 9)
				r = b-a;
			if (get_cur(rpn).type == 8)
				r = a*b;
			if (get_cur(rpn).type == 10)
				r = b/a;
			push(s,r);
		}
		move_next(rpn);
	}
	int val = pop(s);
	Var* v = look_up(vars, res);
	if (v == NULL)
	{
		Var result;
		strcpy_s(result.name, 5, res);
		result.value = val;
		add_first(vars, result);
	}
	else 
		v->value = val;
}

void run_commands(List<Command> cmds, List<Var>& vars)
{
	move_first(cmds);
	while(!is_last(cmds))
	{
		run_command(get_cur(cmds).var_result,get_cur(cmds).rpn, vars);
		move_next(cmds);
	}
}

void interpret(istream& in, Program program, List<Var>& vars) 
{
	read_vars(in, program.in, vars);
	run_commands(program.commands, vars);
}

