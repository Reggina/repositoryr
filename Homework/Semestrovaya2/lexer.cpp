#include "lexer.h"

bool letter(char c)
{
	return (c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z');
}

bool number(char c)
{
	return (c >= '0' && c <= '9');
}

void clear(char a[],int i)
{
	for (int p=0; p<i; p++)
		a[p] = 0;
}


bool lex_analyzer(istream& in, List<Lexeme>& lexemes)
{
	move_first(lexemes);
	char c; int s = 1; int i = 0, l; char num[7] = {0}; char name[5] = {0};
	Lexeme lexer;
	lexer.int_value = 0;
	while ((in.get(c)) && (s != 8) && (s != 10))
	{
 	switch (s)
	{
	case 1: if (letter(c))
			{
				i = 0;
				name[i] = c;
				s = 2;	
			}
			break;
	case 2: if (letter(c) || number(c) || c == ' ')
			{
				if (letter(c) || number(c))
				{
					i++;
					name[i] = c;
				}
				if (c == ' ' )
				{
					while (c == ' ') in.get(c);
					if (c == ':')
						s = 3;
					else 
						s = 10;
				}
			}
			else 
			{
				if (c == ':' || c == ' ') 
				{ 
					while (c == ' ') in.get(c);
					s = 3;
				}
			}
				break;
	case 3: if (c == '=' || c == ' ')
			{
				strcpy_s(lexer.var_name, 5,name);
				lexer.type = 0;
				add_end(lexemes, lexer);
				clear(name,5);
				if (c == ' ') s = 10;
				else
				{
					lexer.type = 2;
					add_end(lexemes, lexer);
					s = 4;
				}
			}
			break;
	case 4: if (c == '+' || c == '-' )
			{
					l = 0;
					num[l] = c;
					s = 6; 
			}
			else 
			{
				if (letter(c)) 
				{
					i = 0;
					name[i] = c;
					s = 7; 
				}
				else 
					if (c == '(')
					{
						lexer.type = 11;
						add_end(lexemes, lexer);
					}
			}
			break;	
	case 6: if (number(c) || c == ' ')
			{
				if (number(c))
				{
					l++;
					num[l] = c;
				}
				else 
				{
					if (c == ' ')
					{
						while (c == ' ') in.get(c);
						if (!number(c))
						{
							lexer.type = 1;
							lexer.int_value = atoi(num);
							add_end(lexemes, lexer);
							lexer.int_value = 0;
							clear(num,7);
							if (c == '+' || c == '-' || c == '*' || c == '/' || c == ' ')
							{
								while (c == ' ') in.get(c);
								if (c == '+') 
									lexer.type = 7;
								if (c == '-')
									lexer.type = 9;
								if (c == '*')
									lexer.type = 8;
								if (c == '/')
									lexer.type = 10;
								add_end(lexemes, lexer);
								s = 4;
							}
							else 
							{
								if (c == ';' )
								{
									lexer.type = 3;
									add_end(lexemes, lexer);
									s = 1;
								}
								else
								{
									if (c == '.')
									{
										lexer.type = 4;
										add_end(lexemes, lexer);
										s = 8;
									}
									else 
										if (c == ')'  )
										{
											lexer.type = 12;
											add_end(lexemes, lexer);
											s = 9;
										}
								}
							}
						}
						else s = 10;
					}
	
				}
			}
			else 
			{
				lexer.type = 1;
				lexer.int_value = atoi(num);
				add_end(lexemes, lexer);
				lexer.int_value = 0;
				clear(num,7);
				if (c == '+' || c == '-' || c == '*' || c == '/' || c == ' ')
				{
					while (c == ' ') in.get(c);
					if (c == '+') 
						lexer.type = 7;
					if (c == '-')
						lexer.type = 9;
					if (c == '*')
						lexer.type = 8;
					if (c == '/')
						lexer.type = 10;
					add_end(lexemes, lexer);
					s = 4;
				}
				else 
				{
					if (c == ';' )
					{
						lexer.type = 3;
						add_end(lexemes, lexer);
						s = 1;
					}
					else
					{
						if (c == '.')
						{
							lexer.type = 4;
							add_end(lexemes, lexer);
							s = 8;
						}
						else 
							if (c == ')'  )
							{
								lexer.type = 12;
								add_end(lexemes, lexer);
								s = 9;
							}
					}
				}
			}
			break;
	case 7: if (letter(c) || number(c) || c == ' ')
			{
				if (letter(c) || number(c))
				{
				i++;
				name[i] = c;
				}
				else 
				{
					if (c == ' ')
					{
						while(c == ' ') in.get(c);
						if (!letter(c) && !number(c))
						{
							strcpy_s(lexer.var_name, 5, name);
							lexer.type = 0;
							add_end(lexemes, lexer);
							clear(name,5); 
			
							if (c == '+' || c == '-' || c == '*' || c == '/' )
							{
								if (c == '+') 
									lexer.type = 7;
								if (c == '-')
									lexer.type = 9;
								if (c == '*')
									lexer.type = 8;
								if (c == '/')
									lexer.type = 10;
								add_end(lexemes, lexer);
								s = 4;
							}
							else 
							{
								if (c == ';' )
								{
									lexer.type = 3;
									add_end(lexemes, lexer);
									s = 1;
								}
								else
								{
									if (c == '.' )
									{
										lexer.type = 4;
										add_end(lexemes, lexer);
										s = 8;
									}
									else 
										if (c == ')')
										{
											lexer.type = 12;
											add_end(lexemes, lexer);
											s = 9;
										}
								}
							}
						
						}
						else s = 10;
					}

				}

			}
			else 
			{
				strcpy_s(lexer.var_name, 5, name);
				lexer.type = 0;
				add_end(lexemes, lexer);
				clear(name,5); 
			
				if (c == '+' || c == '-' || c == '*' || c == '/' )
				{
					if (c == '+') 
						lexer.type = 7;
					if (c == '-')
						lexer.type = 9;
					if (c == '*')
						lexer.type = 8;
					if (c == '/')
						lexer.type = 10;
					add_end(lexemes, lexer);
					s = 4;
				}
				else 
				{
					if (c == ';' )
					{
						lexer.type = 3;
						add_end(lexemes, lexer);
						s = 1;
					}
					else
					{
						if (c == '.' )
						{
							lexer.type = 4;
							add_end(lexemes, lexer);
							s = 8;
						}
						else 
							if (c == ')')
							{
								lexer.type = 12;
								add_end(lexemes, lexer);
								s = 9;
							}
					}
				}
			}
			break;
	case 9: if (c == '+' || c == '-' || c == '*' || c == '/' )
				{
					if (c == '+') 
						lexer.type = 7;
					if (c == '-')
						lexer.type = 9;
					if (c == '*')
						lexer.type = 8;
					if (c == '/')
						lexer.type = 10;
					add_end(lexemes, lexer);
					s = 4;
				}
				else 
				{
					if (c == ';' )
					{
						lexer.type = 3;
						add_end(lexemes, lexer);
						s = 1;
					}
					else
					{
						if (c == '.' )
						{
							lexer.type = 4;
							add_end(lexemes, lexer);
							s = 8;
						}
						else 
							if (c == ')')
							{
								lexer.type = 12;
								add_end(lexemes, lexer);
								s = 9;
							}
					}
			}
			break;
	}
	}
	if (s == 10) return  0;
	if (s == 8) return 1;
}