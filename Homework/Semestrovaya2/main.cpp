#include <iostream>
#include <cstring>

#include "List.h"
#include "types.h"

#include "lexer.h"
#include "syntax.h"
#include "interpreter.h"


using namespace std;


int main() {
  // �������
 List<Lexeme> lexemes;
 create(lexemes);


 
  // ����������� ����������, ������ �� cin, ����� � lexemes

 if (!lex_analyzer(cin, lexemes))
	 cout << "ERROR" << endl;
 else
 {


  // ���������
  Program program;
  init_program(program);

  // �������������� ����������, ���� ������� �� lexemes � ����� ��������� program
 if (!syntax_analyzer(lexemes, program)) 
	 cout << "ERROR";
 else {


  List<Var> vars;
  create(vars);

  interpret(cin, program, vars);

  move_first(vars);
  while (!is_last(vars)) 
  {
	 Var var = get_cur(vars);
     cout << var.name << " = " << var.value << endl;
    
     move_next(vars);
  }
 }
 }


  cout << endl;
  system ("pause");
  return 0;
}




