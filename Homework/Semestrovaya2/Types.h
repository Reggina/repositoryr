#ifndef _TYPES_H_
#define _TYPES_H_

#include "List.h"

struct Lexeme {
  /*
   *  type == 0  - variable
   *  type == 1  - int
   *  type == 2  - assign
   *  type == 3  - semicolon (;)
   *  type == 4  - dot ( )
   *
   *  type == 7  - plus
   *  type == 8  - mult
   *  type == 9  - (-)
   *  type == 10  - (/)
   *  type == 11  - ('(')
   *  type == 12  - (')')
   */
  int type;
  char var_name[100];
  int int_value;
};

struct Command {
  char var_result[100];
  List<Lexeme> rpn;
};

struct Program {
  List<char[100]> in;
  List<Command> commands;
};

struct Var {
  char name[100];
  int value;
};

#endif