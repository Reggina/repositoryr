#ifndef STACK_H
#define STACK_H
#include <iostream>
#include <cstdlib>

template <typename T>
struct elem
{
	T key;
	elem<T>* next;
};

template <typename T>
struct Stack
{
	elem<T>* cur;
};


template <typename T>
void create_stack(Stack<T>& s)
{
	s.cur = NULL;
}

template <typename T>
void push(Stack<T>& s, T x)
{
	elem<T>* p = new elem<T>;
	p->key = x;
	p->next = s.cur;
	s.cur = p;
}

template <typename T>
T pop(Stack<T>& s)
{
	T x = s.cur->key;
	elem<T>* p = s.cur;
	s.cur = s.cur->next;
	delete p; 
	return x;
}

template <typename T>
bool is_empty_stack(Stack<T> s)
{
	return s.cur == NULL;
}


#endif