#ifndef _SYNTAX_H_
#define _SYNTAX_H_

#include "types.h"
#include "List.h"



void init_program(Program& p);
void syntax_analyzer(List<Lexeme> lexemes, Program& program);

#endif
