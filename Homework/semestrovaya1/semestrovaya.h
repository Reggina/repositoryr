#ifndef SEMESTROVAYA_H
#define SEMESTROVAYA_H

struct Node
{
	int elem;
	Node* next;
};

struct List
{
	Node* begin;
	Node* cur;
	Node* end;
};

List createList(int n);
int IF(int n, int k);

#endif