#include <iostream>

#include "semestrovaya.h"

using namespace std;



List createList(int n)
{
	List l;
	l.begin = new Node;
	l.begin->elem = 1;
	l.cur = l.begin;

	for (int i = 2; i <= n; i++)
	{
		l.cur->next = new Node;
		l.cur->next->elem = i;
		l.cur = l.cur->next;
	}
	l.end = l.cur;
	l.cur->next = l.begin;

	return l;
}

int IF(int n, int k)
{
	if (n < 1 || k < 1)
		return -1;
	if (k == 1)
		return n;
	if (n >= 1 && k > 1)
	{
		List l = createList(n);
		l.cur = l.begin;

		while (n > 1)
		{
			for (int i = 1; i < k-1; i++)
				l.cur = l.cur->next;
			Node* b = l.cur;
			l.cur = l.cur->next;
			b->next = l.cur->next;
			delete l.cur;
			l.cur = b->next;
			n--;
		}
		return l.cur->elem;
	}
}