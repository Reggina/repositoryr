#include <gtest/gtest.h>

#include "semestrovaya.h"

TEST(IFTest, test1)
{
	ASSERT_EQ(5, IF(10, 2));
	ASSERT_EQ(10, IF(10, 1));
	ASSERT_EQ(1, IF(1, 10));
	ASSERT_EQ(-1, IF(0, 0));
}

TEST(createListTest, test2)
{
	List l = createList(5);
	ASSERT_TRUE(l.end->next->elem == 1);

}

TEST(createListTest, test3)
{
	List l = createList(1);
	ASSERT_TRUE(l.end->next->elem == 1);
}

TEST(createListTest, test4)
{
	List l = createList(5);
	ASSERT_TRUE(l.end->elem == 5);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);

  
  return RUN_ALL_TESTS(); 
}
