#include <iostream>
using namespace std;

struct Node
{
	int elem;
	Node* next;
};

struct List
{
	Node* begin;
	Node* cur;
	Node*end;
};

List ctreateEmptyList()
{
	List l;
	l.begin = NULL;
	return l;
}

void add_to_end(List &l, int x)
{
	l.cur = l.begin;
	Node* a = new Node;
	a->elem = x;
	a->next = NULL;
	while (l.cur != NULL)
		l.cur = l.cur->next;
	l.cur = a;
}

void move_first(List l)
{
	l.cur = l.begin;
}

void move_next(List l)
{
	if (l.cur != NULL)
		l.cur = l.cur->next;
}

bool is_last(List l)
{
	return l.cur == NULL;
}

int get_cur_val(List l)
{
	if (l.cur != NULL)
		return l.cur->elem;
	return 0;
}

