#include <iostream>
#include "List.h"

using namespace std;

int length(List l);
void print(List l);
List interList(List &l, int x);

int main()
{
	List l;
	ctreateEmptyList(l);

	int n, x;
	cout << "Enter list:";
	cin >> n;
	while (n != 0)
	{
		add_to_end(l, n);
		cin >> n;
	}

	cout << "Enter x:";

	cin >> x;
	interList(l, x);
	print(l);

	cout << endl;
	system ("pause");
	return 0;
}

int length(List l)
{
	int k = 0;
	move_first(l);
	if (l.begin == NULL)
		return 0;
	while (!is_last(l))
	{
		l.cur = l.cur->next;
		k++;
	}

	return k;
}

List interList(List &l, int x)
{
	move_first(l);
	for (int i = 0; i < (length(l)/2)-1; i++)
		l.cur = l.cur->next;
	Node* a = new Node;
	a->elem = x;
	a->next = l.cur->next;
	l.cur->next = a;
	l.cur = l.cur->next->next;
	return l;
}

void print(List l)
{
	move_first(l);
	while (l.cur != NULL)
	{
		cout << get_cur_val(l) << ' ';
		l.cur = l.cur->next;
	}
}