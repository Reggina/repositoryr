#include <iostream>
#include "List.h"

using namespace std;
List intersperse( List &l, int x);
void print(List l);
int main ()
{
	int n, x;

	List l;
	ctreateEmptyList(l);
	
	cin >> n;
	while (n != 0)
	{
		add_to_end(l, n);
		cin >> n;
	}

	cout << "Enter x:";

	cin >> x;

	intersperse( l, x);

	print(l);

	cout << endl;
	system ("pause");
	return 0;
}

List intersperse( List &l, int x)
{
	move_first(l);
	while (l.cur != NULL)
	{
		Node* a = new Node;
		a->elem = x;
		a->next = l.cur->next;
		l.cur->next = a;
		l.cur = l.cur->next->next;
	}

	return l;
}

void print(List l)
{
	move_first(l);
	while (l.cur != NULL)
	{
		cout << get_cur_val(l) << ' ';
		l.cur = l.cur->next;
	}
}